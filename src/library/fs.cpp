// fs.cpp: File System

#include "sfs/fs.h"

#include <algorithm>
#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

// Initialize blocks -----------------------------------------------------------

void FileSystem::initialize_blocks(Block block, Disk *disk){
    
    for(int i = 1; i < block.Super.Blocks; i++){
        //Set a block to empty values
        Block *empty;
        memset(empty, 0, sizeof(Block));
        
        disk->write(i, empty->Data);
    }
}

// Load an inode for given inumber ---------------------------------------------
bool FileSystem::load_inode(Disk *disk, size_t inumber, Inode *node, Block super){
    if(inumber > super.Super.Inodes)
        return false;
    
    Block block;
    int blocknum = inumber / 128;
    if(inumber % 128 != 0 && inumber != 0)
        blocknum += 1;

    disk->read(blocknum, block.Data);
    
    size_t shrunk = inumber;
    if(inumber > 127)
        shrunk = inumber % 128;

    node = &block.Inodes[shrunk];
    
    return true;
}

// Debug file system -----------------------------------------------------------

void FileSystem::debug(Disk *disk) {
    Block block;
    uint32_t InodeBlocks; 
    uint32_t pointers; 

    // Read Superblock
    //get superblock from disk
    disk->read(0, block.Data);


    printf("SuperBlock:\n");
    printf("    %u blocks\n"         , block.Super.Blocks);
    printf("    %u inode blocks\n"   , block.Super.InodeBlocks);
    printf("    %u inodes\n"         , block.Super.Inodes);

    InodeBlocks = block.Super.InodeBlocks;

    // Read Inode blocks
    for(uint32_t i=1;i<=InodeBlocks;i++){
        disk->read(i, block.Data);
        //read each inode in block
        for(uint32_t j=0;j<INODES_PER_BLOCK;j++){
            if(block.Inodes[j].Size != 0){
                printf("Inode%u:\n", (j));
                printf("size: %u bytes\n", block.Inodes[j].Size);

                pointers = 0;
                for(uint32_t k=0;k<POINTERS_PER_INODE;k++){
                    if(block.Inodes[j].Direct[k] != 0){
                        pointers++;
                         printf("direct blocks: %u\n", block.Inodes[j].Direct[k]);

                    }
                }
            }
        }
    }
}

// Format file system ----------------------------------------------------------

bool FileSystem::format(Disk *disk){
    //check if already mounted
    if(disk->mounted())
        return false;

    //Write super block
    Block block;
    block.Super.MagicNumber = MAGIC_NUMBER;
    block.Super.Blocks = disk->size();
    if(ceil(disk->size() * 0.1) >= 1)
        block.Super.InodeBlocks = ceil(disk->size() * 0.1);
    else
        block.Super.InodeBlocks = 1;
    block.Super.Inodes = block.Super.InodeBlocks * INODES_PER_BLOCK;

    disk->write(0,block.Data);
    
    //Clear all other blocks
    initialize_blocks(block, disk);

    return true;
}

// Mount file system -----------------------------------------------------------

bool FileSystem::mount(Disk *disk){
    //Sanity checks
    if(disk->mounted())
        return false;
    
    Block block;
    disk->read(0, block.Data);

    int regular_blocks = block.Super.Blocks - (1 + block.Super.InodeBlocks);
    int offset = 1 + block.Super.InodeBlocks;
    //Set the bitmaps with all false;
    InodeBitmap.resize(block.Super.Inodes, false);
    DataBitmap.resize(regular_blocks, false);
    
    char test[32];
    memset(test, 0, sizeof(test));

    Inode *node;
    //Set the bitmaps appropriately
    for(uint32_t i=1;i<=InodeBlocks;i++){
        disk->read(i, block.Data);
        for(uint32_t j=0;j<INODES_PER_BLOCK;j++){
            if(block.Inodes[j].Valid == 1)
            //TODO locate correct address in bit map
                InodeBitmap[] = true;
    }

    char test2[4096];
    memset(test2, 0, sizeof(4096));

    for(size_t i = 0; i < DataBitmap.size(); i++){
        
        disk->read((i + offset), block.Data);
        if(memcmp(test2, &block, sizeof(test)) != 0)
            DataBitmap[i] = true;
    }

    disk->mount();
    return true;
}

// Create inode ----------------------------------------------------------------

ssize_t FileSystem::create() {
    // Locate free inode in inode table

    // Record inode if found
    return 0;
}

// Remove inode ----------------------------------------------------------------

bool FileSystem::remove(size_t inumber) {
    // Load inode information

    // Free direct blocks

    // Free indirect blocks

    // Clear inode in inode table
    return true;
}

// Inode stat ------------------------------------------------------------------

ssize_t FileSystem::stat(size_t inumber) {
    // Load inode information
    return 0;
}

// Read from inode -------------------------------------------------------------

ssize_t FileSystem::read(size_t inumber, char *data, size_t length, size_t offset) {
    // Load inode information

    // Adjust length

    // Read block and copy to data
    return 0;
}

// Write to inode --------------------------------------------------------------

ssize_t FileSystem::write(size_t inumber, char *data, size_t length, size_t offset) {
    // Load inode
    
    // Write block and copy to data
    return 0;
}
